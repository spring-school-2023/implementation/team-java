package com.festo.springschool.warehouse.utils;

public class CellHelper {

    private static final Integer X_CROSSING_CAPACITY = 4;
    private static final Integer T_CROSSING_CAPACITY = 6;
    private static final Integer CORNER_CAPACITY = 9;
    private static final Integer HALLWAY_CAPACITY = 8;
    private static final Integer DEAD_END_CAPACITY = 12;

    public static final Integer getCellCapacity(Boolean northWall, Boolean southWall, Boolean eastWall, Boolean westWall) {
        Integer capacity = null;

        if(checkIfXCrossing(northWall, southWall, eastWall, westWall)) {
            capacity = X_CROSSING_CAPACITY;
        } else if (checkIfTCrossing(northWall, southWall, eastWall, westWall)) {
            capacity = T_CROSSING_CAPACITY;
        } else if (checkIfCorner(northWall, southWall, eastWall, westWall)) {
            capacity = CORNER_CAPACITY;
        } else if (checkIfHallway(northWall, southWall, eastWall, westWall)) {
            capacity = HALLWAY_CAPACITY;
        } else if (checkIfDeadEnd(northWall, southWall, eastWall, westWall)) {
            capacity = DEAD_END_CAPACITY;
        }
        return capacity;
    }

    public static boolean checkIfXCrossing(Boolean northWall, Boolean southWall, Boolean eastWall, Boolean westWall) {
        Boolean checkIfXCrossing = false;

        if(Boolean.FALSE.equals(northWall) && Boolean.FALSE.equals(southWall)
                && Boolean.FALSE.equals(eastWall) && Boolean.FALSE.equals(westWall)) {
            checkIfXCrossing = true;
        }
        return checkIfXCrossing;
    }

    public static boolean checkIfTCrossing(Boolean northWall, Boolean southWall, Boolean eastWall, Boolean westWall) {
        Boolean checkIfTCrossing = false;

        if(Boolean.TRUE.equals(northWall) && Boolean.FALSE.equals(southWall)
                && Boolean.FALSE.equals(eastWall) && Boolean.FALSE.equals(westWall)) {
            checkIfTCrossing = true;
        }
        return checkIfTCrossing;
    }

    public static boolean checkIfCorner(Boolean northWall, Boolean southWall, Boolean eastWall, Boolean westWall) {
        Boolean checkIfCorner = false;

        if(Boolean.TRUE.equals(northWall) && Boolean.FALSE.equals(southWall)
                && Boolean.FALSE.equals(eastWall) && Boolean.TRUE.equals(westWall)) {
            checkIfCorner = true;
        }
        return checkIfCorner;
    }

    public static boolean checkIfHallway(Boolean northWall, Boolean southWall, Boolean eastWall, Boolean westWall) {
        Boolean checkIfHallway = false;

        if(Boolean.FALSE.equals(northWall) && Boolean.FALSE.equals(southWall)
                && Boolean.TRUE.equals(eastWall) && Boolean.TRUE.equals(westWall)) {
            checkIfHallway = true;
        }
        return checkIfHallway;
    }

    public static boolean checkIfDeadEnd(Boolean northWall, Boolean southWall, Boolean eastWall, Boolean westWall) {
        Boolean checkIfDeadEnd = false;

        if(Boolean.TRUE.equals(northWall) && Boolean.FALSE.equals(southWall)
                && Boolean.TRUE.equals(eastWall) && Boolean.TRUE.equals(westWall)) {
            checkIfDeadEnd = true;
        }
        return checkIfDeadEnd;
    }

}
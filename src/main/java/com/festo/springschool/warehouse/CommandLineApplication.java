package com.festo.springschool.warehouse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.festo.springschool.warehouse.cli.CommandLineHandler;
import com.festo.springschool.warehouse.configuration.spring.CommandLineConfiguration;
import com.festo.springschool.warehouse.model.CommandLineInterface;

@Configuration
@ComponentScan(basePackageClasses = {CommandLineConfiguration.class})
public class CommandLineApplication
        implements CommandLineRunner {

    @Autowired
    private CommandLineHandler commandLineHandler;

    @Override
    public void run(final String... args)
            throws Exception {
        commandLineHandler.handle(args);
    }

    public static void main(final String[] args)
            throws Exception {
         
        SpringApplication.run(CommandLineApplication.class, args);

        CommandLineInterface cli = new CommandLineInterface();
        try{
            cli.intializeApplication(args[0]);
        }catch(Exception e){
            cli.intializeApplication("");
            cli.waitForCommands();
        }
    }

}

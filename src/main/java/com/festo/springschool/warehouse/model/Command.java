package com.festo.springschool.warehouse.model;

public interface Command {
    public String getCommand();

    public String execute(String[] args);
}

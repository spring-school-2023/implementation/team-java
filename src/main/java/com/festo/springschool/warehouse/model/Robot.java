package com.festo.springschool.warehouse.model;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;


public class Robot {

    String robotname;
    String direction;
    String baseurl = "http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com/";
    HttpClient client;
    
    
    public Robot(String name) {
    	this.robotname = name;
    	initHttpClient();
    }

    private void initHttpClient() {
    	client = HttpClient.newHttpClient();
	
	}

	/*
     * This function calls the api function to move robot in a particular direction
     */
    public boolean move(String direction) {
    	if(callMoveApi(direction) == 200 ) {
    		return true;
    	}
    	else
    		return false;

    }
    
    /*
     * This function calls the api function to do a scan near for the robot 
     * length specifies near or far
     */
    public boolean scan(String length) {
    	if(callScanApi(length) == 200 ) {
    		return true;
    	}
    	else
    		return false;

    }


    protected int callScanApi(String length) {
        String uri = baseurl + "api/bot/" + robotname + "/scan/" + length  ;

        int responsestatuscode = 0;
        try {
     	   
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(uri))
                    .build();	

             HttpResponse<String> response =
                   client.send(request, BodyHandlers.ofString());
             responsestatuscode = response.statusCode();
             System.out.println(response.statusCode());

         } catch (ProtocolException e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
         } catch (IOException e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
         } catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
     	return responsestatuscode;    	
	}
    
    protected int callResetBotPosition() {
        String uri = baseurl + "api/bot/" + robotname + "/reset"  ;
        int responsestatuscode = 0;
        try {
     	   
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(uri))
                    .PUT(BodyPublishers.noBody())
                    .build();	

             HttpResponse<String> response =
                   client.send(request, BodyHandlers.ofString());
             responsestatuscode = response.statusCode();
             System.out.println(response.statusCode());

         } catch (ProtocolException e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
         } catch (IOException e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
         } catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
     	return responsestatuscode;    	
    }

	protected int callMoveApi(String direction) {
       String uri = baseurl + "api/bot/" + robotname + "/move/" + direction  ;
       int responsestatuscode = 0;
    	 
    	
       try {
    	   
           HttpRequest request = HttpRequest.newBuilder()
                   .uri(URI.create(uri))
                   .PUT(BodyPublishers.noBody())
                   .build();	

            HttpResponse<String> response =
                  client.send(request, BodyHandlers.ofString());
            responsestatuscode = response.statusCode();
            System.out.println(response.statusCode());

        } catch (ProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return responsestatuscode;
    }

    protected int checkBotApi(String baseurl) {
        String uri = baseurl + "api/bot/"+robotname + "/" ;
    	//String uri = "http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com/api/bot/judy/";
        
     	int responsestatuscode = 0;
     	     	
        try {
        	
         	HttpClient client = HttpClient.newHttpClient();
             HttpRequest request = HttpRequest.newBuilder()
                   .uri(URI.create(uri))
                   .build();

             HttpResponse<String> response =
                     client.send(request, BodyHandlers.ofString());
               responsestatuscode = response.statusCode();
               

         } catch (ProtocolException e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
         } catch (IOException e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
         } catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
     	return responsestatuscode;
     }
    

}

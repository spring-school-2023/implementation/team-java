package com.festo.springschool.warehouse.model;

import com.festo.springschool.warehouse.utils.CellHelper;

public class Cell {

    private Boolean northWall;
    private Boolean southWall;
    private Boolean eastWall;
    private Boolean westWall;

    private Integer capacity;

    public Cell(Boolean northWall, Boolean southWall,
            Boolean eastWall, Boolean westWall) {
        this.northWall = northWall;
        this.southWall = southWall;
        this.eastWall = eastWall;
        this.westWall = westWall;
        this.capacity = CellHelper.getCellCapacity(northWall, southWall, eastWall, westWall);
    }

    public Boolean getNorthWall() {
        return northWall;
    }

    public Boolean getSouthWall() {
        return southWall;
    }

    public Boolean getEastWall() {
        return eastWall;
    }

    public Boolean getWestWall() {
        return westWall;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setNorthWall(Boolean northWall) {
        this.northWall = northWall;
    }

    public void setSouthWall(Boolean southWall) {
        this.southWall = southWall;
    }

    public void setEastWall(Boolean eastWall) {
        this.eastWall = eastWall;
    }

    public void setWestWall(Boolean westWall) {
        this.westWall = westWall;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }
}

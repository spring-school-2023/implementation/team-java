package com.festo.springschool.warehouse.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CommandLineInterface{

    private String robotName;
    private Scanner input = new Scanner(System.in);
    private HashMap<String, Command> availableCommands = new HashMap<String, Command>();
    
    public CommandLineInterface(){
        availableCommands.put("move", new CommandMove());
        availableCommands.put("easteregg", new CommandEasteregg());
    }

    public boolean intializeApplication(String robotNameInput){       
        try{
            while(robotName == null){
                if(checkIfRobotNameIsValid(robotNameInput)){
                    System.out.println("Initializing robot "+robotName);
                }else{
                    System.out.println("Please enter a valid robot name (maxine, martha, laurie, kimberly, ken, kathleen, judy, joseph)");
                    robotNameInput = input.next();
                }
            }

            return true;
        }catch(Exception e){
            return false;
        }
    }

    public void waitForCommands(){
        String userInput = "";
        while(true){
            userInput = input.nextLine();
            callCommand(userInput);
        }
    }

    protected boolean checkIfRobotNameIsValid(String userInput){
        String[] possibleRobotNames = {"maxine", "martha", "laurie", "kimberly", "ken", "kathleen", "judy", "joseph"};

        if(Arrays.asList(possibleRobotNames).contains(userInput)){
            robotName = userInput;
            return true;
        }else{
            return false;
        }       
    }

    protected boolean callCommand(String userInput){
        try{
            String userCommand = userInput.split(" ")[0];
            String userCommandArguments[] = Arrays.copyOfRange(userInput.split(" "), 1, userInput.split(" ").length);
            availableCommands.get(userCommand).execute(userCommandArguments);
            return true;
        }catch(NullPointerException e){
            System.out.println("Please enter a valid command:");
            
            for(Map.Entry<String, Command> actualCommand: availableCommands.entrySet()){
                System.out.println("    "+actualCommand.getValue().getCommand());
            }

            return false;
        }

        
    }


    
}
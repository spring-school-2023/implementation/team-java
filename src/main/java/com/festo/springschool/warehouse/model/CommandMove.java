package com.festo.springschool.warehouse.model;

public class CommandMove implements Command {

    @Override
    public String getCommand() {
        return "move [north | south | west | east]";
    }

    @Override
    public String execute(String[] args) {
        if(args[0].equals("north") || args[0].equals("south") || args[0].contains("west") || args[0].contains("east")){
            //Call robot.move(args[0])
            return "Robot Moved";
        }else{
            return "Wrong Statement";
        }
    }
    
}
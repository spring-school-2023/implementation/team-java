package com.festo.springschool.warehouse.model;

public class HelloWorld {

    public String greet(String name) {
        return String.format("Hello %s", name);
    }

}

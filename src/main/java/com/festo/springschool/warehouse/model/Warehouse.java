package com.festo.springschool.warehouse.model;

import java.util.ArrayList;

public class Warehouse {
    private ArrayList<ArrayList<Cell>> map;
    private int capacity;
    private int robotsPosition[] = new int[2];

    public void initializeMap(){
        map = new ArrayList<ArrayList<Cell>>();
        map.add(new ArrayList<Cell>());
        map.get(0).add(new Cell(false, false, false, false));
        robotsPosition[0] = 0;
        robotsPosition[1] = 0;
    }
    
    public boolean updateMap(Cell cell, int horizontalCoordinate, int verticalCoordinate){
        Cell cellInMap = map.get(verticalCoordinate).get(horizontalCoordinate);
        
        
        return true;
    }

    public ArrayList<ArrayList<Cell>> getMap(){
        return map;
    }

    public int getCapacity(){
        return capacity;
    }

    public int getHorizontalCellCount(){
        return 0;
    }

    public int getVerticalCellCount(){
        return 0;
    }

    public void saveMap(String filename){
    }

    public void loadMap(String filename){
    }

    public int[] getRobotsPosition(){
        return robotsPosition;
    }


}

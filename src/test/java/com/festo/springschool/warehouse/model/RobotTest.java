package com.festo.springschool.warehouse.model;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.net.http.HttpHeaders;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RobotTest {

    Robot robotUnderTest;
    String baseurl = "http://springschool-lb-54580289.eu-central-1.elb.amazonaws.com/";

    

    @BeforeEach
    public void setUpEach() {
        robotUnderTest = new Robot("judy");
        robotUnderTest.callResetBotPosition();
    }
    
    @Test
    public void testCheckCallApiForMove() {
     	assertThat(robotUnderTest.callMoveApi("east")).isIn(200, 405);
    }    

    @Test
    public void testCheckCallApiForScan() {
     	assertEquals(robotUnderTest.callScanApi("near"), 200);
     	assertEquals(robotUnderTest.callScanApi("far"), 200);
    }  
    
    @Test
    public void testCheckBotApiJudy() {
     	assertEquals(robotUnderTest.checkBotApi(baseurl), 200);
     }
    
    @AfterEach
    public void TearDown() {
        robotUnderTest.callResetBotPosition();
    }
    

}
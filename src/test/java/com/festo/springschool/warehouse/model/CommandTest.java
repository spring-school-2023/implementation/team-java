package com.festo.springschool.warehouse.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CommandTest {
    
    private Command underTest;

    @BeforeEach
    public void setUpEach() {
       
    }

    @Test
    public void testMoveCommandWorks() {
        underTest = new CommandMove();
        assertTrue(!underTest.getCommand().isEmpty());
        
        String[] args = {"north"};
        assertTrue(underTest.execute(args).equals("Robot Moved"));
    }
}

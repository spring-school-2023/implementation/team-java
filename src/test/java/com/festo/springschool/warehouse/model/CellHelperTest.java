package com.festo.springschool.warehouse.model;

import com.festo.springschool.warehouse.utils.CellHelper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CellHelperTest {

    @BeforeEach
    public void setUpEach() {}

    @Test
    public void checkIfXCrossingTest() {
        Assertions.assertTrue(CellHelper.checkIfXCrossing(false, false, false, false));
        Assertions.assertFalse(CellHelper.checkIfXCrossing(true, true, true, true));
        Assertions.assertFalse(CellHelper.checkIfXCrossing(false, false, false, true));
        Assertions.assertFalse(CellHelper.checkIfXCrossing(false, false, true, false));
        Assertions.assertFalse(CellHelper.checkIfXCrossing(false, true, false, false));
        Assertions.assertFalse(CellHelper.checkIfXCrossing(true, false, false, false));
    }

    @Test
    public void checkIfTCrossingTest() {
        Assertions.assertTrue(CellHelper.checkIfTCrossing(true, false, false, false));
        Assertions.assertFalse(CellHelper.checkIfTCrossing(true, true, true, true));
        Assertions.assertFalse(CellHelper.checkIfTCrossing(false, false, false, false));
        Assertions.assertFalse(CellHelper.checkIfTCrossing(false, false, true, false));
        Assertions.assertFalse(CellHelper.checkIfTCrossing(false, true, false, false));
        Assertions.assertFalse(CellHelper.checkIfTCrossing(false, true, true, false));
    }

    @Test
    public void checkIfCorner() {
        Assertions.assertTrue(CellHelper.checkIfCorner(true, false, false, true));
        Assertions.assertFalse(CellHelper.checkIfCorner(true, true, true, true));
        Assertions.assertFalse(CellHelper.checkIfCorner(false, false, false, false));
        Assertions.assertFalse(CellHelper.checkIfCorner(false, false, true, false));
        Assertions.assertFalse(CellHelper.checkIfCorner(false, true, false, false));
        Assertions.assertFalse(CellHelper.checkIfCorner(true, false, false, false));
    }

    @Test
    public void checkIfHallway() {
        Assertions.assertTrue(CellHelper.checkIfHallway(false, false, true, true));
        Assertions.assertFalse(CellHelper.checkIfHallway(true, true, true, true));
        Assertions.assertFalse(CellHelper.checkIfHallway(false, false, false, false));
        Assertions.assertFalse(CellHelper.checkIfHallway(false, false, true, false));
        Assertions.assertFalse(CellHelper.checkIfHallway(false, true, false, false));
        Assertions.assertFalse(CellHelper.checkIfHallway(true, false, false, false));
    }

    @Test
    public void checkIfDeadEnd() {
        Assertions.assertTrue(CellHelper.checkIfDeadEnd(true, false, true, true));
        Assertions.assertFalse(CellHelper.checkIfDeadEnd(true, true, true, true));
        Assertions.assertFalse(CellHelper.checkIfDeadEnd(false, false, false, false));
        Assertions.assertFalse(CellHelper.checkIfDeadEnd(false, false, true, false));
        Assertions.assertFalse(CellHelper.checkIfDeadEnd(false, true, true, false));
        Assertions.assertFalse(CellHelper.checkIfDeadEnd(true, false, false, false));
    }


}

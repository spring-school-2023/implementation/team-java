package com.festo.springschool.warehouse.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CommandLineInterfaceTest {
    
    private CommandLineInterface underTest;

    @BeforeEach
    public void setUpEach() {
        underTest = new CommandLineInterface();
    }

    @Test
    public void testDoesCheckIfRobotNameIsValidWorks() {
        assertTrue(underTest.checkIfRobotNameIsValid("maxine"));
        assertFalse(underTest.checkIfRobotNameIsValid("falseString"));
        assertFalse(underTest.checkIfRobotNameIsValid(null));
    }

    @Test
    public void testDoesintializeApplicationWorks() {
        assertTrue(underTest.intializeApplication("maxine"));
    }

    @Test
    public void testDoesCallCommandWorks() {
        assertFalse(underTest.callCommand("false String"));
        assertTrue(underTest.callCommand("move south"));
        assertFalse(underTest.callCommand(null));
    }
    

}

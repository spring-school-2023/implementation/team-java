package com.festo.springschool.warehouse.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.InstanceOf;

import com.festo.springschool.warehouse.model.Warehouse;
import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

public class WarehouseTest {
    
    private Warehouse underTest;

    @BeforeEach
    public void setUpEach() {
       underTest = new Warehouse();
    }

    @Test
    public void testInitialization(){
        underTest.initializeMap();
        assertNotNull(underTest.getMap());
    }

    @Test
    public void testUpdateMap(){
        assertTrue(underTest.updateMap(new Cell(true, false, true, false), 0, 0));
        assertTrue(underTest.updateMap(new Cell(null, false, true, false), 0, 0));
    }

    @Test
    public void testGetMap(){
        assertNotNull(underTest.getMap());
    }

    @Test
    public void testGetCapacity(){
        assertThat(underTest.getCapacity());
        assertNotNull(underTest.getCapacity());
    }

    @Test
    public void testGetHorizontalCellCount(){
        assertThat(underTest.getHorizontalCellCount());
        assertNotNull(underTest.getHorizontalCellCount());
    }

    @Test
    public void testGetVerticalCellCount(){
        assertThat(underTest.getVerticalCellCount());
        assertNotNull(underTest.getVerticalCellCount());
    }

    @Test
    public void testSaveAndLoadMap(){
        underTest.initializeMap();
        assertTrue(underTest.updateMap(new Cell(true, false, true, false), 0, 0));
        underTest.saveMap("Test.map");
        ArrayList<ArrayList<Cell>> OriginalMap = underTest.getMap();

        underTest.initializeMap();
        underTest.loadMap("Test.map");
        ArrayList<ArrayList<Cell>> LoadedMap = underTest.getMap();

        assertNotNull(LoadedMap);
        assertEquals(OriginalMap, LoadedMap);
    }

    @Test
    public void testGetRobotsPosition(){
        assertTrue(underTest.getRobotsPosition().length >= 2);
    }


}

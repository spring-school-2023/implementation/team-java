package com.festo.springschool.warehouse.model;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class WarehousePlotterTest {
    private WarehousePlotter underTestWarehousePlotter = new WarehousePlotter();

    @Test
    public void TestintializeWarehouseToPrint()
    {
        assertNotNull(underTestWarehousePlotter.intializeWarehouseToPrint()); 
    }

    @Test 
    public void TestgetPositionRobot()
    {
        int[] testresult = new int[2]; 
        testresult[0] = 0; 
        testresult[1] = 0; 
        int[] actualresult = underTestWarehousePlotter.getPositionRobot(); 
        assertEquals(testresult[0], actualresult[0]); 
        assertEquals(testresult[1], actualresult[1]); 

    }


}
